import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { User } from '../Model/User';



@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public user:User[];
  private baseUri ='http://localhost:8080/';

  constructor(private http:HttpClient, private router:Router) { }
  private registerUri = `${this.baseUri}/register`;
  private loginUri = `${this.baseUri}/login`;

  userRegistration(body){
    //http methods return observables
    return this.http.post(this.registerUri,body);
  }
  userLogin(body){
    //http methods return observables
    return this.http.post(this.loginUri,body);
  }
  
  isUserLoggedIn(){
    if(sessionStorage.getItem('token')){
      return true;
    }else{
      return false;
    }
  }

  logOut(){
    sessionStorage.clear();
    sessionStorage.removeItem('token');
    this.router.navigateByUrl('/auth/login');
  }
}

