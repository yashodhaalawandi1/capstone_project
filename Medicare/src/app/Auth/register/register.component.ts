import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public registerForm: any;
  constructor(private authSrv: AuthService,
    private router: Router,
    private formBuilder: FormBuilder) {
    this.createForm();
  }

  ngOnInit() {
  }
  createForm() {
    this.registerForm = this.formBuilder.group({
      Name:'',
      email: '',
      username: '',
      password: '',     
      
    });
  }
  register(): void {
    

    this.authSrv.userRegistration(this.registerForm.value).
      subscribe(res => {
        if (res== "400") {
          console.log("Details cannot be empty");
        } else {
          this.router.navigate(['/login']);
        }
      },
        err => {
          alert("An error has occured, Please try again !!!");
        });
  }
}