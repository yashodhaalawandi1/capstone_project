import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm = new FormGroup({
    username: new FormControl(''),
    password: new FormControl('')})

  constructor(private authSrv:AuthService,private router:Router) { }

  ngOnInit(): void {
  }

  onSubmit()
  {
    this.authSrv.userLogin(this.loginForm.value).subscribe(
      res=>
      {
        sessionStorage.setItem('token',res['token']);
       alert("your login is success")
        this.router.navigateByUrl('Products/productlist');
      },err=>{
        console.log(err);
        alert("Invalid credentials, please retry");
      }
    )
    
  }

}
