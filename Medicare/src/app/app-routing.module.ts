import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { MedicinesComponent } from './admin/medicines/medicines.component';
import { UsersComponent } from './admin/users/users.component';
import { AuthModule } from './Auth/auth.module';
import { HomeComponent } from './home/home.component';
import { ProductListComponent } from './Products/product-list/product-list.component';
import { ProductsModule } from './Products/products.module';

const routes: Routes = [
  {path:"", component:HomeComponent},
  {path:"home", component:HomeComponent},
  {path:"Auth", loadChildren:()=>import('./Auth/auth.module').then(m=>m.AuthModule)},
  { path: 'admin/users', component: UsersComponent },
  { path: 'admin/medicines', component: MedicinesComponent },
  {path:'Products', loadChildren:()=>import('./Products/products.module').then(m=>m.ProductsModule)},
  { path: 'admin/users', component: UsersComponent },


];

@NgModule({
  imports: [RouterModule.forRoot(routes),AuthModule,FormsModule,ProductsModule],
  exports: [RouterModule],
  
})
export class AppRoutingModule { }
