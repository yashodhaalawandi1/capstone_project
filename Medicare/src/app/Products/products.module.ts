import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductListComponent } from './product-list/product-list.component';
import { Router, RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';

const routes:Routes=[
  {path:"productlist", component:ProductListComponent},


]


@NgModule({
  declarations: [ProductListComponent],
  imports: [
    CommonModule,RouterModule.forChild(routes),FormsModule
  ]
})
export class ProductsModule { }
