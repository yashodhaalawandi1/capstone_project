import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from './Auth/auth.service';
import { AuthModule } from './Auth/auth.module';

import { HttpClientModule } from '@angular/common/http';
import { UsersComponent } from './admin/users/users.component';
import { SearchComponent } from './search/search.component';
import { MedicinesComponent } from './admin/medicines/medicines.component';
import { AddmedicineComponent } from './admin/medicines/addmedicine/addmedicine.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    HomeComponent,
    UsersComponent,
    SearchComponent,
    MedicinesComponent,
    AddmedicineComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AuthModule  
      ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
