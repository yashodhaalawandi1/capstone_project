import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Medicines } from 'src/app/Model/Medicines';
import { HttpClientService } from 'src/app/service/http-client.service';

@Component({
  selector: 'app-medicines',
  templateUrl: './medicines.component.html',
  styleUrls: ['./medicines.component.css']
})
export class MedicinesComponent implements OnInit {

  medicines:
  Array<Medicines>;
  selectedMedicine:Medicines;
  action:string;

  constructor(private httpClientService: HttpClientService,private activedRoute:ActivatedRoute,private router:Router) { }

  ngOnInit(){
    this.refreshData();
  }
  
    refreshData() {
      this.httpClientService.getMedicines().subscribe(
        response => this.handleSuccessfulResponse(response)
      );
      this.activedRoute.queryParams.subscribe(
        (params) => {
          this.action = params['action'];
        }
      );
    }
  
  handleSuccessfulResponse(response) {
    this.medicines = response;
  }

  addMedicine() {
    this.selectedMedicine = new Medicines();
    this.router.navigate(['admin', 'medicines'], { queryParams: { action: 'add' } });
  }


}
