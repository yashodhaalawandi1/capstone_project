import { Component, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { Medicines } from '../Model/Medicines';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  querySubscribe: Subscription;
  page: number = 0;
  keyword: string;
  canFetch: boolean = false;

  // products: ProductDisplay[] = [];


  constructor() { }

  ngOnInit(): void {
  }

  filterData:Medicines;

  filterData1=[

    {

      id:1,
    name:'Sumo',
   Description:'Sumo Tablet is a combination medicine which helps in relieving pain',
   uses:'fever',
   price:50,
   expDate:'12-12-202',
   createDate:'12-12-2022',
   companyName:'Cipla'
      
    }
  ]

//   @Output() searchcriteria = new EventEmitter<String>();
// searchThis() {
//     this.searchcriteria.emit(this.searchword)
// }
// newArray
//   searchThis(data) {
//     this.content = this.newArray
//     console.log(data)
//     if (data) {
//       this.content = this.content.filter(function (ele, i, array) {
//         let arrayelement = ele.name.toLowerCase()
//         return arrayelement.includes(data)
//       })
//     }
//     else {
//       console.log(this.content)
//     }
//     console.log(this.content)
//   }
// }

}
