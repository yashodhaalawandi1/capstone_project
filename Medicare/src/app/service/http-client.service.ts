import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Medicines } from '../Model/Medicines';
import { User } from '../Model/User';

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {

  constructor(private httpClient:HttpClient) { }

  getUsers()
  {
    return this.httpClient.get<User[]>('http://localhost:8080/users/');
  }

  addUser(newUser: User) {
    return this.httpClient.post<User>('http://localhost:8080/register', newUser);   
  }
  deleteUser(id) {
    return this.httpClient.delete<User>('http://localhost:8080/user/' + id);
  }

  getMedicines()
  {
          return this.httpClient.get<Medicines[]>('http://localhost:8080/medicines');
    }

    addMedicine(newMedicine: Medicines) {
      return this.httpClient.post<Medicines>('http://localhost:8080/add', newMedicine);
    }
  
}
