package com.org.medicare.Medicare.Resource;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.org.medicare.Medicare.Repository.MedicineRepo;
import com.org.medicare.Medicare.model.Medicine;
import com.org.medicare.Medicare.serviceImp.MedicineSrvImp;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class MedicineResource {
	
	private byte[] bytes;
	
	@Autowired
    private MedicineRepo medrepo;
	
	    @GetMapping("/medicines")
    public List<Medicine> getAllMedicines() {
        return medrepo.findAll();
    }

    @GetMapping("/medicines/{id}")
    public ResponseEntity<Optional<Medicine>> getMedicineById(@PathVariable(value = "id") Long Id)
    {
        Optional<Medicine> medicine = medrepo.findById(Id); 
        return ResponseEntity.ok().body(medicine);
    }
    @PostMapping("/upload")
	public void uploadImage(@RequestParam("imageFile") MultipartFile file) throws IOException {
		this.bytes = file.getBytes();
	}
    
    
    @PostMapping("/add")
    public void addMedicine(@RequestBody Medicine medicine) throws IOException{
    	medicine.setImageURI(this.bytes);
		medrepo.save(medicine);
		this.bytes = null;
    }

//    @PutMapping("/medicines/{id}")
//    public ResponseEntity<Medicine> updateMedicine(@PathVariable(value = "id") Long Id,
//         @RequestBody Medicine medicine) {
//    	
//    	
//    	Medicine updatedmedicine=msimp.updateMedicine(Id, medicine);
//		/*
//		 * Medicine employee = medrepo.findById(Id);
//		 * employee.setEmailId(employeeDetails.getEmailId());
//		 * employee.setLastName(employeeDetails.getLastName());
//		 * employee.setFirstName(employeeDetails.getFirstName()); final Employee
//		 * updatedEmployee = employeeRepository.save(employee); return
//		 * ResponseEntity.ok(updatedEmployee);
//		 */
//		 
//    	
//    	return ResponseEntity.ok(updatedmedicine);
//    }

    @DeleteMapping("/medicines/{id}")
    public Map<String, Boolean> deleteEmployee(@PathVariable(value = "id") Long Id)
          {
        Optional<Medicine> medicine = medrepo.findById(Id);
        medrepo.deleteById(Id);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
	

}
