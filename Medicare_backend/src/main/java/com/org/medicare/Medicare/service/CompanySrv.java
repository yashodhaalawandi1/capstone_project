package com.org.medicare.Medicare.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.org.medicare.Medicare.model.Company;
@Service
public interface CompanySrv {
	
	public void addCompany(Company c);
	
	
	public void deleteCompany(int id);
	
	
	public List<Company> getCompanies();
	
	public Optional<Company> getCompanybyId(int id);

	
	}
