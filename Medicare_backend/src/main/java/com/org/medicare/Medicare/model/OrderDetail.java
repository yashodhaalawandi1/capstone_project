package com.org.medicare.Medicare.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Order_Details")
public class OrderDetail  {
	
	 @Id
	 @Column(name = "ID", length = 50, nullable = false)
	 private String id;
	 
	 @ManyToOne(fetch = FetchType.LAZY)
	    @JoinColumn(name = "ORDER_ID", nullable = false)
//	    		referencedColumnName="ORDER_DETAIL_ORD")
	    private Orders order;
	 
	 @ManyToOne(fetch = FetchType.LAZY)
	    @JoinColumn(name = "PRODUCT_ID", nullable = false)
	    private Medicine medicines;
	 
	 private int quantity;
	 
	 private double price;
	 private double amount;
	 
	 
	
	 public OrderDetail() {}
	 
		public OrderDetail(String id, Orders order, Medicine medicines, int quantity, double price, double amount) {
		super();
		this.id = id;
		this.order = order;
		this.medicines = medicines;
		this.quantity = quantity;
		this.price = price;
		this.amount = amount;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Orders getOrder() {
		return order;
	}
	public void setOrder(Orders order) {
		this.order = order;
	}
	public Medicine getMedicines() {
		return medicines;
	}
	public void setMedicines(Medicine medicines) {
		this.medicines = medicines;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "OrderDetail [id=" + id + ", order=" + order + ", medicines=" + medicines + ", quantity=" + quantity
				+ ", price=" + price + ", amount=" + amount + "]";
	}
	
	
	 
	 
	 
	

}
