package com.org.medicare.Medicare.Resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.org.medicare.Medicare.model.Company;
import com.org.medicare.Medicare.serviceImp.CompanySrvImp;

@RestController
public class CompanyResource {
	
	@Autowired
	private CompanySrvImp csrv;
	
	@GetMapping("/companies")
	
	public List<Company> getCompanyList(){
		
	return (List<Company>)csrv.getCompanies();
		
	}
	
	@PostMapping("/Company")
	public void add(@RequestBody Company c) {
		csrv.addCompany(c);
	}
	
	@GetMapping("/company/{id}")
	public Optional<Company> getCompanyById(@PathVariable(required=true) int id) {
		
		return csrv.getCompanybyId(id);
		
	}
	
	@DeleteMapping("company/{id}")
	public void removeCompany(@PathVariable(required=true) int id)
	{
	 csrv.deleteCompany(id);
	}
	
	

}
