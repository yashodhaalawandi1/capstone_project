package com.org.medicare.Medicare.service;

import java.util.List;
import java.util.Optional;


import com.org.medicare.Medicare.model.Medicine;


public interface MedicineSrv {
	
public void addMedicine(Medicine medicine);
	
	
	public void deleteMedicine(long id);
	
	
	public List<Medicine> getMedicine();
	
	public Optional<Medicine> getMedicineById(long id);


	Medicine updateMedicine(Long id, Medicine medicine);


}
