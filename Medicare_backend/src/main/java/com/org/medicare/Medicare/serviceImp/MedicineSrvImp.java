package com.org.medicare.Medicare.serviceImp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.medicare.Medicare.Repository.MedicineRepo;
import com.org.medicare.Medicare.model.Medicine;
import com.org.medicare.Medicare.model.User;
import com.org.medicare.Medicare.service.MedicineSrv;

@Service
public class MedicineSrvImp implements MedicineSrv {
	
	@Autowired
	MedicineRepo mrepo;

	@Override
	public void addMedicine(Medicine medicine) {
		mrepo.save(medicine);
		
	}

	@Override
	public void deleteMedicine(long id) {
		mrepo.deleteById(id);
		
	}

	@Override
	public List<Medicine> getMedicine() {
		return mrepo.findAll();
	}

	@Override
	public Optional<Medicine> getMedicineById(long id) {
		
		return mrepo.findById(id);
	}
	
	@Override
	public Medicine updateMedicine(Long id,Medicine medicine) {

		if (mrepo.findById(id).isPresent()){
            Medicine existingmed= mrepo.findById(id).get();

            existingmed.setId(medicine.getId());
            existingmed.setDescription(medicine.getDescription());
            existingmed.setImageURI(medicine.getImageURI());
            existingmed.setName(medicine.getName());
            existingmed.setPrice(medicine.getPrice());
            existingmed.setCompanyName(medicine.getCompanyName());
            

            Medicine updatedMed = mrepo.save(existingmed);
         
            return new Medicine(updatedMed.getId(),updatedMed.getName(),updatedMed.getDescription(),updatedMed.getUses(),updatedMed.getPrice(),
            		updatedMed.getImageURI(),updatedMed.getCreateDate(),updatedMed.getExpDate(),updatedMed.getCompanyName());
        }else{
            return null;
        }
    }
	
	

}
