package com.org.medicare.Medicare.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.boot.context.properties.ConstructorBinding;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Setter
@Getter
@ToString
@ConstructorBinding
public class Company {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
   private	int id;
   private String cname;
   private Date created;
   private Date endDate;
   private String profile;

   public Company() {}

public Company(int id, String cname, Date created, Date endDate, String profile) {
	super();
	this.id = id;
	this.cname = cname;
	this.created = created;
	this.endDate = endDate;
	this.profile = profile;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getCname() {
	return cname;
}

public void setCname(String cname) {
	this.cname = cname;
}

public Date getCreated() {
	return created;
}

public void setCreated(Date created) {
	this.created = created;
}

public Date getEnded() {
	return endDate;
}

public void setEnded(Date endDate) {
	this.endDate = endDate;
}

public String getProfile() {
	return profile;
}

public void setProfile(String profile) {
	this.profile = profile;
}

@Override
public String toString() {
	return "Company [id=" + id + ", cname=" + cname + ", created=" + created + ", endDate=" + endDate + ", profile="
			+ profile + "]";
}
   
   
   
   
   
	}
