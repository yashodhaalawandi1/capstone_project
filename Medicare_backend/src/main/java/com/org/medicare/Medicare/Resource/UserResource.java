package com.org.medicare.Medicare.Resource;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.xml.ws.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.org.medicare.Medicare.Repository.UserRepo;
import com.org.medicare.Medicare.model.User;

import com.org.medicare.Medicare.serviceImp.UserDetailSrv;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class UserResource {
	@Autowired
	private UserRepo urepo;


    @Autowired 
	private UserDetailSrv usrv;
    
    
    
	
	@GetMapping("/users")
	
	public List<User> getUsersList(){
		
	return (List<User>)urepo.findAll();
		
	}
	
//	@PostMapping("/login")
//	public String verifyUser(Model model) {
//		
//		return "Login";
//	}
	
	@PostMapping("/register")
	public User add(@RequestBody User user) {
		return urepo.save(user);
	}
	
	@GetMapping("/user/{id}")
	public Optional<User> getUserById(@PathVariable(required=true) long id) {
		return urepo.findById(id);
		
	}
	
	/*
	 * @PutMapping("/user/{id}") public ResponseEntity<User> editUser(@RequestBody
	 * User newUser, @PathVariable Long id) { return new
	 * ResponseEntity<>(vehicleCommandService.updateVehicle(id, vehicleUpdateDTO),
	 * HttpStatus.OK); }
	 */
//	 @DeleteMapping("user/{id}") public void
//	 removeCompany(@PathVariable(required=true) long id) 
//	 { usrv.deleteUser(id); }
	 
	 @DeleteMapping("user/{id}" )
		public User deleteUser(@PathVariable("id") long id) {
			User user = urepo.getOne(id);
			urepo.deleteById(id);
			return user;
		}
	 
	
	
	@GetMapping("/403")
	public String error403() {
		
		return "403";
		
	}
}