package com.org.medicare.Medicare.model;

public class CartLine {
	
	private Medicine medicine;
	
	private int quantity;
	
	public CartLine()
	{
		this.quantity=0;
	}
	
	public Medicine getMedicine()
	
	{
		return medicine;
	}
	
	public void setMedicine(Medicine medicine)
	{
		this.medicine=medicine;
	}
	
	public int getQuantity()
	{
		return quantity;
	}
	
	public int setQuantity(int quantity)
	{
		return this.quantity=quantity;
	}
	
	public double getAmount()
	{
		return this.medicine.getPrice()*this.quantity;
	}

}
