package com.org.medicare.Medicare.serviceImp;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.org.medicare.Medicare.Repository.UserPrinciples;
import com.org.medicare.Medicare.Repository.UserRepo;
import com.org.medicare.Medicare.model.Role;
import com.org.medicare.Medicare.model.User;


@Service
public class UserDetailSrv implements UserDetailsService {

	@Autowired
	private UserRepo urepo;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		User user = urepo.findUserByUsername(username);

		if (user == null) {
			throw new UsernameNotFoundException("User doesn't exists");
		}
		Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        for (Role role : user.getRoles()){
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
        }
		return new UserPrinciples(user);
		    }

	public void deleteUser(long id) {
		urepo.deleteById(id);
		
	}


	
//
//	public void addUser(User user) {
//		urepo.save(user);
//
//	}
//
//	public void deleteUser(long id) {
//		urepo.deleteById(id);
//	}
//
//	public List<User> getUsers() {
//		return urepo.findAll();
//	}
//
//	public Optional<User> getUserById(long id) {
//		return urepo.findById(id);
//	}

}
