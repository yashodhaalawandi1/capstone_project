package com.org.medicare.Medicare.Repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.org.medicare.Medicare.model.Role;
import com.org.medicare.Medicare.model.User;


@Repository
public interface UserRepo extends JpaRepository<User, Long> {

	User findByEmailAndPassword(String username, String password);

	User findUserByUsername(String username);

	

}
