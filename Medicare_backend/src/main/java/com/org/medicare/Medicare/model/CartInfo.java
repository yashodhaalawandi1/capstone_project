package com.org.medicare.Medicare.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

public class CartInfo {
	
	private int orderNum;
	 
	@Autowired
    private Customer customer;
 
    private final List<CartLine> cartLines = new ArrayList<CartLine>();
 
    public CartInfo() {
 
    }

	public int getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(int orderNum) {
		this.orderNum = orderNum;
	}

	public Customer getCustomerInfo() {
		return customer;
	}

	public void setCustomerInfo(Customer customer) {
		this.customer = customer;
	}

	public List<CartLine> getCartLines() {
		return cartLines;
	}
	
	private CartLine findLineByCode(long id) {
        for (CartLine line : this.cartLines) {
        	
            if (line.getMedicine().getId()==id) {
                return line;
            }
        }
        return null;
    }
 
	
	public void addMedicine(Medicine medicine, int quantity) {
        CartLine line = this.findLineByCode(medicine.getId());
 
        if (line == null) {
            line = new CartLine();
            line.setQuantity(0);
            line.setMedicine(medicine);
            this.cartLines.add(line);
        }
        int newQuantity = line.getQuantity() + quantity;
        if (newQuantity <= 0) {
            this.cartLines.remove(line);
        } else {
            line.setQuantity(newQuantity);
        }
    }
    
	public void validate() {
		 
    }
	
	public void updateMedicine(Long id, int quantity)
	{
		CartLine line=this.findLineByCode(id);
		if(line!=null) {
			if(quantity<=0)
			{
				this.cartLines.remove(line);
				
			}else
				line.setQuantity(quantity);
		}
	}
    
	public void removeMedicine(Medicine medicine) {
		CartLine line=this.findLineByCode(medicine.getId());
		if(line!=null)
		{
			this.cartLines.remove(line);
		}
		
	}
	
	public boolean isEmpty() {
		return this.cartLines.isEmpty();
		
	}
	
	 public boolean isValidCustomer() {
	        return this.customer!= null && this.customer.isValid();
	    }
	 
	 public int getQuantityTotal() {
	        int quantity = 0;
	        for (CartLine line : this.cartLines) {
	            quantity += line.getQuantity();
	        }
	        return quantity;
	    }
	 
	 public double getAmountTotal() {
	        double total = 0;
	        for (CartLine line : this.cartLines) {
	            total += line.getAmount();
	        }
	        return total;
	    }
	 
	 public void updateQuantity(CartInfo cart) {
	        if (cart != null) {
	            List<CartLine> lines = cart.getCartLines();
	            for (CartLine line : lines) {
	                this.updateMedicine(line.getMedicine().getId(), line.getQuantity());
	            }
	        }
	 
	    }
}
