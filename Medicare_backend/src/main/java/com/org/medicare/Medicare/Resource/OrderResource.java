package com.org.medicare.Medicare.Resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.org.medicare.Medicare.Repository.OrderRepo;
import com.org.medicare.Medicare.model.Orders;
import com.org.medicare.Medicare.model.User;
import com.org.medicare.Medicare.service.OrderSrv;
import com.org.medicare.Medicare.serviceImp.OrderSrvImp;

@CrossOrigin
@RestController
public class OrderResource {
	
	@Autowired
    private OrderRepo orepo;
	
	@Autowired
	OrderSrv osrv;
	
	@Autowired
OrderSrvImp osimp;

    @GetMapping("/order")
    public List<Orders> getAllOrders() {
        return orepo.findAll();
    }

    @GetMapping("/order/{id}")
	public Optional<Orders> getUserById(@PathVariable(required=true) long id) {
		return  osimp.findById(id);
    }
		
    
    @PostMapping("/order/add")
    public Orders addMedicine(@RequestBody Orders order) {
        return orepo.save(order);
    }
    
    
	
	

}
