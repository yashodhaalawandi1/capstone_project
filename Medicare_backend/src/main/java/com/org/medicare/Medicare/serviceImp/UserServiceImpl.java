//package com.org.medicare.Medicare.serviceImp;
//
//import java.util.HashSet;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.stereotype.Service;
//
//import com.org.medicare.Medicare.Repository.RoleRepository;
//import com.org.medicare.Medicare.Repository.UserRepo;
//import com.org.medicare.Medicare.model.User;
//import com.org.medicare.Medicare.service.UserService;
//@Service
//	public class UserServiceImpl implements UserService {
//	    @Autowired
//	    private UserRepo userRepository;
//	    @Autowired
//	    private RoleRepository roleRepository;
//	    @Autowired
//	    private BCryptPasswordEncoder bCryptPasswordEncoder;
//
//	    @Override
//	    public void save(User user) {
//	        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
//	        user.setRoles(new HashSet<>(roleRepository.findAll()));
//	        userRepository.save(user);
//	    }
//
//	    @Override
//	    public User findByUsername(String username) {
//	        return userRepository.findUserByUsername(username);
//	    }
//
//	}
//
