package com.org.medicare.Medicare.service;

public interface SecurityService {
	
	 String findLoggedInUsername();

	    void autoLogin(String username, String password);

}
