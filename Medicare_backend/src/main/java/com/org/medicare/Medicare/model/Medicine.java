package com.org.medicare.Medicare.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity

public class Medicine {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="Medicine_id")
	private long id;
	
	private String name;
	private String Description;
	private String uses;
	private double price;
	private String expDate;
	private String createDate;
	
	private byte[] imageURI;
	private String companyName;
	
	

	public Medicine() {}
	
	

	public Medicine(long id, String name, String description, String uses, double price, byte[] imageURI,
			String expDate,String createDate, String companyName) {
		super();
		this.id = id;
		this.name = name;
		Description = description;
		this.uses = uses;
		this.price = price;
		this.imageURI = imageURI;
		this.expDate=expDate;
		this.createDate=createDate;
		this.companyName=companyName;
	}



	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getUses() {
		return uses;
	}

	public void setUses(String uses) {
		this.uses = uses;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}



	public byte[] getImageURI() {
		return imageURI;
	}



	public void setImageURI(byte[] imageURI) {
		this.imageURI = imageURI;
	}



	public String getExpDate() {
		return expDate;
	}



	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}



	public String getCreateDate() {
		return createDate;
	}



	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}



	public String getCompanyName() {
		return companyName;
	}



	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	

	
	
	
	

}
