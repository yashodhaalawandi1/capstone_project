package com.org.medicare.Medicare.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Bean
	public AuthenticationProvider authProvider() {
		
		DaoAuthenticationProvider provider=new DaoAuthenticationProvider();
		provider.setUserDetailsService(userDetailsService);
		provider.setPasswordEncoder(NoOpPasswordEncoder.getInstance());
		return provider;
	}
	
	
	
	  @Override 
	  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	  
	  auth.authenticationProvider(authProvider());
	  
		/*
		 * inMemoryAuthentication()
		 * .withUser("user").password("{noop}password").roles("USER") .and()
		 * .withUser("admin").password("{noop}password").roles("ADMIN","USER");
		 */
	  }
	  
	/*
	 * @Override protected void configure(HttpSecurity http) throws Exception {
	 * 
	 * http.authorizeRequests() .antMatchers("/delete/**").hasAuthority("ADMIN")
	 * .antMatchers("/edit/**").hasAnyAuthority("ADMIN","EDITOR")
	 * .anyRequest().authenticated() .and() .formLogin().permitAll() .and()
	 * .logout().permitAll() .and() .exceptionHandling().accessDeniedPage("/403"); }
	 */
	  /*http.httpBasic().and() .authorizeRequests()
	  .antMatchers(HttpMethod.GET,"/users/**").hasRole("USER")
	  .antMatchers(HttpMethod.POST,"/users/**").hasRole("ADMIN")
	  .antMatchers(HttpMethod.PUT,"/users/**").hasRole("ADMIN")
	  .antMatchers(HttpMethod.DELETE,"/users/**").hasRole("ADMIN") .and()
	  .csrf().disable() .formLogin().disable(); }
	 */
	  
	  @Override
	  protected void configure(HttpSecurity http) throws Exception {
	      http.csrf().and()
	              .authorizeRequests()
	              .antMatchers("/admin/**").authenticated()//.hasAnyRole("ADMIN","USER")
	              .and().formLogin().loginPage("/login").permitAll()
	              .and().logout();
	      http.csrf().disable();
	      http.headers().frameOptions().disable();
	  }

}
