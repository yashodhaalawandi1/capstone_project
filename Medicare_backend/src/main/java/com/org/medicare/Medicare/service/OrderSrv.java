package com.org.medicare.Medicare.service;

import java.util.List;
import java.util.Optional;

import com.org.medicare.Medicare.model.Medicine;
import com.org.medicare.Medicare.model.Orders;

public interface OrderSrv {

	
	public void deleteOrder(long id);
	
	
	public List<Orders> getOrder();
	
	public Optional<Orders> getOrderById(String id);

	Orders updateOrder(long id, Orders order);
	
	



}
