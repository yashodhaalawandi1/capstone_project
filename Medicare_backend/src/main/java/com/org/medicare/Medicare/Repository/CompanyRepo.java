package com.org.medicare.Medicare.Repository;



import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import com.org.medicare.Medicare.model.Company;

@Repository
public interface CompanyRepo extends JpaRepository<Company, Integer>{

	

}
