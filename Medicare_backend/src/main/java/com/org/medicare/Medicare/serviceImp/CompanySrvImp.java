package com.org.medicare.Medicare.serviceImp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.org.medicare.Medicare.Repository.CompanyRepo;
import com.org.medicare.Medicare.model.Company;
import com.org.medicare.Medicare.service.CompanySrv;

@Service
public class CompanySrvImp implements CompanySrv{
	@Autowired
	CompanyRepo crepo;
	
	public void addCompany(Company c)
	{
	crepo.save(c);
	
	}
	
	public void deleteCompany(int id)
	{
		crepo.deleteById(id);
	}
	
	public List<Company> getCompanies(){
		return crepo.findAll();
	}
	
	public Optional<Company> getCompanybyId(int id) {
		return crepo.findById(id);
		}

	
	}
