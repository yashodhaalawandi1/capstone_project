package com.org.medicare.Medicare.service;

import com.org.medicare.Medicare.model.User;

public interface UserService {
	    void save(User user);

	    User findByUsername(String username);
	}

