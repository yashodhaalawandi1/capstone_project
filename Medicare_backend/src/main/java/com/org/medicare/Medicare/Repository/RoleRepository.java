package com.org.medicare.Medicare.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.org.medicare.Medicare.model.Role;

public interface  RoleRepository extends JpaRepository<Role, Long>{ 
	
	

}
