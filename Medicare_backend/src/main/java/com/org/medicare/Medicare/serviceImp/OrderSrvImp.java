package com.org.medicare.Medicare.serviceImp;

import java.util.List;
import java.util.Optional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.medicare.Medicare.Repository.OrderRepo;
import com.org.medicare.Medicare.model.Medicine;
import com.org.medicare.Medicare.model.Orders;
import com.org.medicare.Medicare.service.OrderSrv;

@Service
public class OrderSrvImp implements OrderSrv {
	@Autowired
	OrderRepo orderrepo;
	
	/*
	 * @Autowired private SessionFactory sessionFactory;
	 */

	@Override
	public void deleteOrder(long id) {
		orderrepo.deleteById(id);

	}

	@Override
	public List<Orders> getOrder() {
		return orderrepo.findAll();
		}

//	@Override
//	public Optional<Orders> getOrderById(String id) {
//		return orderrepo.findById(id);
//	}

	@Override
	public Orders updateOrder(long id, Orders order) {
		if (orderrepo.findById(id).isPresent()){
            Orders existingOrder= orderrepo.findById(id).get();

            existingOrder.setId(existingOrder.getId());
            existingOrder.setAmount(existingOrder.getAmount());
            existingOrder.setCustomerAddress(existingOrder.getCustomerAddress());;
            existingOrder.setCustomerEmail(existingOrder.getCustomerEmail());
            existingOrder.setCustomerName(existingOrder.getCustomerName());
            existingOrder.setCustomerPhone(existingOrder.getCustomerPhone());
            existingOrder.setOrderDate(existingOrder.getOrderDate());
            existingOrder.setOrderNum(existingOrder.getOrderNum());
            

            Orders updatedOrder = orderrepo.save(existingOrder);
         
            return new Orders(updatedOrder.getId(),updatedOrder.getOrderDate(),updatedOrder.getOrderNum(),
            		updatedOrder.getAmount(),updatedOrder.getCustomerName(),updatedOrder.getCustomerAddress(),
            		updatedOrder.getCustomerEmail(),
            		updatedOrder.getCustomerPhone());
        }else
            return null;
        
		
	}

	public Optional<Orders> findById(long id) {
		return orderrepo.findById(id);
	}

	@Override
	public Optional<Orders> getOrderById(String id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/*
	 * private int getMaxOrderNum() { String sql = "Select max(o.orderNum) from " +
	 * Orders.class.getName() + " o "; Session session =
	 * this.sessionFactory.getCurrentSession(); Query<Integer> query=
	 * session.createQuery(sql, Integer.class); Integer value = (Integer)
	 * query.getSingleResult(); if (value == null) { return 0; } return value; }
	 * 
	 * int orderNum = this.getMaxOrderNum() + 1;
	 * 
	 * 
	 */
}
