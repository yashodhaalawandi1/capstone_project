package com.org.medicare.Medicare.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.org.medicare.Medicare.model.Medicine;

@Repository
public interface MedicineRepo extends JpaRepository<Medicine, Long> {

}
